/**

QINEPT-CT 

Quantitative INEPT, constant time, dual modulation. Simplified version with some developement aids removed.

Parameters:
	nt		transients, min 32 (4 phases 8 delays)
	pp		proton pulse
	pplvl		proton power
	gt1-gt3		PFG lengths
	gzlvl1-3	PFG powers
	
Flags:
	gpurge		n|y	
	gpurge180	n|y
	seqmod		a|b|n
	cpulsetype	a|b|c|d
	printpars	y|n


Valtteri Mäkelä / University of Helsinki

*/

#include <standard.h>

static char VERSION_STRING[MAXSTR] = "qineptct_v6_2016_04_29";

/* phasetables */
static int phi_1[4] = {1,3,1,3};
static int phi_2[4] = {0,0,2,2};
static int phi_rec[4] = {0,2,2,0};

/* delay tables */
static double delta1[8];
static double delta1_a[8];
static double delta1_b[8];
static double delta1_c[8];

static double delta2[8];
static double delta2_a[8];
static double delta2_b[8];
static double delta2_c[8];

/** 
* create composite inversion or regular hard pulse (in inept). Type d should be used.
*/
void composite_inv(double pw, char type) {
	if(type=='a') {
		rgpulse(pw*(90.0/90), zero, rof1, 0.0);
		rgpulse(pw*(180.0/90), one, 0.0, 0.0);
		rgpulse(pw*(90.0/90), zero, 0.0, rof2);
	} else 
	if(type=='b') {
		/* 7 x pw */
		rgpulse(pw*(90.0/90), zero, rof1, 0.0);
		rgpulse(pw*(225.0/90), two, 0.0, 0.0);
		rgpulse(pw*(315.0/90), zero, 0.0, rof2);
	} else 
	if(type=='c') {
		/* 6.19667 x pw */
		rgpulse(pw*(38.2/90), zero, rof1, 0.0);
		rgpulse(pw*(110.7/90), two, 0.0, 0.0);
		rgpulse(pw*(159.3/90), zero, 0.0, 0.0);
		rgpulse(pw*(249.5/90), two, 0.0, rof2);
	} else 
	if(type=='d') {
		/* shaka6 10.933333333 x pw */
		rgpulse(pw*158.0/90.0, zero, rof1, 0.0);
		rgpulse(pw*171.2/90.0, two, 0.0, 0.0);
		rgpulse(pw*342.8/90.0, zero, 0.0, 0.0);
		rgpulse(pw*145.5/90.0, two, 0.0, 0.0);
		rgpulse(pw*81.2/90.0, zero, 0.0, 0.0);
		rgpulse(pw*85.3/90.0, two, 0.0, rof2);
	}
	else {
		rgpulse(pw*2.0, zero, rof1, rof2);
	}
}

/** create composite inversion or regular hard pulse (in refoc inept). Type d should be used. */
void composite_refoc(double pw, char type) {
	if(type=='a') {
		rgpulse(pw*(90.0/90), zero, rof1, 0.0);
		rgpulse(pw*(180.0/90), one, 0.0, 0.0);
		rgpulse(pw*(90.0/90), zero, 0.0, rof2);
	} else 
	if(type=='b') {
		/* 16.1 x 90 */
		rgpulse(pw*(336.0/90), zero, rof1, 0.0);
		rgpulse(pw*(246.0/90), two, 0.0, 0.0);
		
		rgpulse(pw*(10.0/90), one, 0.0, 0.0);
		rgpulse(pw*(74.0/90), three, 0.0, 0.0);
		rgpulse(pw*(10.0/90), one, 0.0, 0.0);
		
		rgpulse(pw*(246.0/90), two, 0.0, 0.0);
		rgpulse(pw*(336.0/90), zero, 0.0, rof2);
	} else if(type=='c') {
		/* 12 x 90 */
		rgpulse(pw*(360.0/90), one, rof1, 0.0);
		rgpulse(pw*(270.0/90), three, 0.0, 0.0);
		rgpulse(pw*(90.0/90), two, 0.0, 0.0);
		
		rgpulse(pw*(360.0/90), zero, 0.0, 0.0);
		rgpulse(pw*(270.0/90), two, 0.0, 0.0);
		rgpulse(pw*(90.0/90), one, 0.0, rof2);
	} else if(type=='d') {
		/* shaka 6 */
		rgpulse(pw*158.0/90.0, zero, rof1, 0.0);
		rgpulse(pw*171.2/90.0, two, 0.0, 0.0);
		rgpulse(pw*342.8/90.0, zero, 0.0, 0.0);
		rgpulse(pw*145.5/90.0, two, 0.0, 0.0);
		rgpulse(pw*81.2/90.0, zero, 0.0, 0.0);
		rgpulse(pw*85.3/90.0, two, 0.0, rof2);
	}
	else {
		rgpulse(pw*2.0, zero, rof1, rof2);
	}
}

/** create variable delay from given delay table */
void delta_delay(double delta[], double modifier) {
	
	/* mod8 counter, for 4-step phase cycle */
	
	assign(ct,v2);
	
	/* for 4-step phasecycle */
	hlv(v2,v2); 
	hlv(v2,v2); /* 0000 1111 2222 ... */
	
	/* for 8 step delay modulation */ 
	initval(8,v8); /* mod8 */ 
	modn(v2,v8,v2); /* 0000 1111 2222 ... 7777 0000 ... */
	
	ifzero(v2);
		delay(delta[0] + modifier);
	endif(v2);
	decr(v2);

	ifzero(v2);
		delay(delta[1] + modifier);
	endif(v2);
	decr(v2);
	
	ifzero(v2);
		delay(delta[2] + modifier);
	endif(v2);
	decr(v2);
	
	ifzero(v2);
		delay(delta[3] + modifier);
	endif(v2);
	decr(v2);
	
	ifzero(v2);
		delay(delta[4] + modifier);
	endif(v2);
	decr(v2);
	
	ifzero(v2);
		delay(delta[5] + modifier);
	endif(v2);
	decr(v2);
	
	ifzero(v2);
		delay(delta[6] + modifier);
	endif(v2);
	decr(v2);
	
	ifzero(v2);
		delay(delta[7] + modifier);
	endif(v2);
}

void pulsesequence() {
	
	/* -------------- variables ------------- */
	
	/* flags */
	char seq_mod[MAXSTR];
	char cpulse_type[MAXSTR];
	char gpurge[MAXSTR];
	char gpurge180[MAXSTR];
	char printpars[MAXSTR];
	
	/* PFG */
	double gzlvl1 = 0;
	double gzlvl2 = 0;
	double gzlvl3 = 0;
	double gzlvl4 = 0;
	double gt1 = 0;
	double gt2 = 0;
	double gt3 = 0;
	double gt4 = 0;
	
	/* pulses */
	double pp;
	double pplvl;
	
	/* delays */
	double gdelay;
	
	double modpulse_a_delay;
	double modpulse_b_delay;
	
	double delta1_max;
	double delta2_max;
	double delta_max_adddelay;
	
	/* parameters & other */
	int i;
	int pulseseqmod_ok;
	
	
	/* -------------- get / calc variables ------------- */
	
	/* --- static parameters --- */
	
	gdelay = 150.0e-6;
	delta_max_adddelay = 0.300e-3;
	
	/* --- flags --- */
	
	getstr("gpurge", gpurge);
	getstr("gpurge180", gpurge180);
	getstr("seqmod", seq_mod);
	getstr("cpulsetype", cpulse_type);
	getstr("printpars", printpars);
		
	/* --- gradients --- */
	
	if(gpurge[0] == 'y') {
		gzlvl1 = getval("gzlvl1");
		gt1 = getval("gt1");
	}
	
	if(gpurge180[0] == 'y') {
		gzlvl2 = getval("gzlvl2");
		gzlvl3 = getval("gzlvl3");	
		gt2 = getval("gt2");
		gt3 = getval("gt3");
	}
	
	gzlvl4 = getval("gzlvl4");	
	gt4 = getval("gt4");
	
	/* --- pulses --- */
	
	pp = getval("pp");
	pplvl = getval("pplvl");
	
	/* --- phases --- */
	
	settable(t1,4,phi_1);
	settable(t2,4,phi_2);
	settable(t31,4,phi_rec);
	
	/* --- delays --- */
	pulseseqmod_ok = 0;

	if(seq_mod[0] == 'o') {
		/* 115-170 3.64% */
		delta1[0] = 1/(2*167.53);
		delta1[1] = 1/(2*237.67);
		delta1[2] = 1/(2*231.58);
		delta1[3] = 1/(2*98.29);
		delta1[4] = 1/(2*205.64);
		delta1[5] = 1/(2*260.44);
		delta1[6] = 1/(2*234.43);
		delta1[7] = 1/(2*56.63);
		
		delta2[0] = 1/(2*139.41);
		delta2[1] = 1/(2*411.2);
		delta2[2] = 1/(2*430.99);
		delta2[3] = 1/(2*133.88);
		delta2[4] = 1/(2*174.96);
		delta2[5] = 1/(2*425.94);
		delta2[6] = 1/(2*440.88);
		delta2[7] = 1/(2*115.45);
		
		pulseseqmod_ok = 1;
		
	} else if(seq_mod[0] == 't') {
		/* 115-170 12.7% */
		delta1[0] = 5.0000e-3;
		delta1[1] = 5.0000e-3;
		delta1[2] = 2.2748e-3;
		delta1[3] = 2.0373e-3;
		delta1[4] = 1.5143e-3;
		delta1[5] = 1.0079e-3;
		delta1[6] = 1.0000e-3;
		delta1[7] = 1.0000e-3;
		
		delta2[0] = 3.4146e-3;
		delta2[1] = 3.4146e-3;
		delta2[2] = 1.8994e-3;
		delta2[3] = 1.5505e-3;
		delta2[4] = 1.5079e-3;
		delta2[5] = 1.5113e-3;
		delta2[6] = 1.7575e-3;
		delta2[7] = 1.4250e-3;
		
		pulseseqmod_ok = 1;
	} else if(seq_mod[0] == 'g') {
		/* genetic */
		delta1[0] = 2.1955e-3;
		delta1[1] = 1.9759e-3;
		delta1[2] = 1.9657e-3;
		delta1[3] = 2.0504e-3;
		delta1[4] = 4.7903e-3;
		delta1[5] = 8.9549e-3;
		delta1[6] = 4.0376e-3;
		delta1[7] = 1.9959e-3;
		
		delta2[0] = 2.7088e-3;
		delta2[1] = 1.2350e-3;
		delta2[2] = 1.2291e-3;
		delta2[3] = 1.2121e-3;
		delta2[4] = 3.7006e-3;
		delta2[5] = 4.4513e-3;
		delta2[6] = 3.7339e-3;
		delta2[7] = 1.1984e-3;
		
		pulseseqmod_ok = 1;
	} else if(seq_mod[0] == 'r') {
		/* genetic time restricted */
		delta1[0] = 5.5000e-3;
		delta1[1] = 1.5283e-3;
		delta1[2] = 1.4814e-3;
		delta1[3] = 1.5419e-3;
		delta1[4] = 1.4037e-3;
		delta1[5] = 1.5982e-3;
		delta1[6] = 5.5000e-3;
		delta1[7] = 1.4943e-3;
		
		delta2[0] = 3.5836e-3;
		delta2[1] = 1.7226e-3;
		delta2[2] = 1.8069e-3;
		delta2[3] = 1.7993e-3;
		delta2[4] = 1.7661e-3;
		delta2[5] = 1.8304e-3;
		delta2[6] = 3.5999e-3;
		delta2[7] = 1.8390e-3;
		
		pulseseqmod_ok = 1;	
	} else {
		for(i=0; i<8;i++) {
			delta1[i] = 0.0;
			delta2[i] = 0.0;
		}
	}
	
	/* determine longest delays */
	delta1_max = 0;
	delta2_max = 0;
	
	for(i=0; i<8;i++) {
		if(delta1[i] > delta1_max) 
			delta1_max = delta1[i];
		if(delta2[i] > delta2_max) 
			delta2_max = delta2[i];
	}
	
	delta1_max += delta_max_adddelay;
	delta2_max += delta_max_adddelay;

	/* pulse lenghts to be added to evolution */
	
	modpulse_a_delay = pw * 2.0;
	modpulse_b_delay = pp * 2.0;
	
	/* composite pulse lenghts to be added to the first evolution */
	if(cpulse_type[0]=='a') {
		modpulse_a_delay = pw * 4.0;
	} else if(cpulse_type[0]=='b') {
		modpulse_a_delay = pw * 7.0;
	} else if(cpulse_type[0]=='c') {
		modpulse_a_delay = pw * 6.19667;
	} else if(cpulse_type[0]=='d') {
		modpulse_a_delay = pw * 10.933333;
	} 

	/* calculate delta1_a/b and delta2_a/b from delta delay arrays */ 
	for (i=0; i<8; i++) {
		delta1_a[i] = delta1[i] / 2.0;
		delta1_b[i] = delta1_max / 2.0 - delta1_a[i];
		delta1_c[i] = delta1_max / 2.0;
		
		delta2_a[i] = delta2[i] / 2.0;
		delta2_b[i] = delta2_max / 2.0 - delta2_a[i];
		delta2_c[i] = delta2_max / 2.0;
	}
	
	/* --- printout --- */
	
	printf("\n%s\n\n",VERSION_STRING);
	
	if(printpars[0] == 'y') {
		printf("pw = %1.2fus @ %1.0fdb \n", pw*1e6, tpwr);
		printf("pp = %1.2fus @ %1.0fdb  \n", pp*1e6, pplvl);
		printf("dpwr = %1.0fdb \n", dpwr); 
		
		printf("composite pulse in delta1 (cpulsetype[0]): "); 
		if (cpulse_type[0] == 'a' || cpulse_type[0] == 'b' || cpulse_type[0] == 'c' || cpulse_type[0] == 'd') {
			printf("yes, type = %c \n", cpulse_type[0] );
		} else {
			printf("no \n");
		}
		
		printf("composite pulse in delta2 (cpulsetype[1]): ");
		if (cpulse_type[1] == 'a' || cpulse_type[1] == 'b' || cpulse_type[1] == 'c' || cpulse_type[1] == 'd') {
			printf("yes, type = %c \n", cpulse_type[1] );
		} else {
			printf("no \n");
		}
		
		printf("\n");
		printf("Purge mangetization (gpurge) = ");
		if (gpurge[0] == 'y') {
			printf("yes\n");
		} else {
			printf("no\n");
		}
		
		printf("Purge gradients around inversion and refocusing pulses (gpurge180) = ");
		if (gpurge180[0] == 'y') {
			printf("yes\n");
		} else {
			printf("no\n");
		}
		
		printf("\nPulse sequence modulation: ");
		if (pulseseqmod_ok == 1) {
			printf("yes, using modulation %c \n", seq_mod[0]);
			printf("delta1_max %1.3fms\t delta2_max %1.3fms\t \n", delta1_max*1e3, delta2_max*1e3);
			
			printf("\nvariable delays (ms) \n"); 
			printf("# d_1a\t d1_b\t d_1c\t total\t d_2a\t d_2b\t d_2c\t total\n"); 
			
			for (i=0; i<8; i++) {
				printf("%u ", i);
				printf("%1.3f\t ", delta1_a[i]*1e3);
				printf("%1.3f\t ", delta1_b[i]*1e3);
				printf("%1.3f\t ", delta1_c[i]*1e3);
				printf("%1.3f\t ", (delta1_a[i] + delta1_b[i] + delta1_c[i])*1e3);
				printf("%1.3f\t ", delta2_a[i]*1e3);
				printf("%1.3f\t ", delta2_b[i]*1e3);
				printf("%1.3f\t ", delta2_c[i]*1e3);
				printf("%1.3f\n", (delta2_a[i] + delta2_b[i] + delta2_c[i] )*1e3);
			}
			
		} 
		
		printf("gdelay = %1.0fus, ", gdelay*1e6);
		printf("rof1 = %1.3fms, rof2 = %1.3fms\n", rof1*1e3, rof2*1e3);
	}
	
	/* --- checks --- */
	
	for (i=0; i<8; i++) {
		/* eg. 250us gradient + 150us grecovery + 100us max for composite = 500us */ 
		if (delta1_a[i] < (rof2 + gt2 + rof1 + modpulse_a_delay * 0.5) ) {
			printf("\nError: delta1_a #%d too short\n", i);
		}
		
		if (delta1_b[i] < (rof2 + rof1 + modpulse_a_delay * 0.5) ) {
			printf("\nError: delta1_b #%d too short\n", i);
		}
		
		if (delta2_a[i] < (rof2 + gt3 + rof1 + modpulse_b_delay * 0.5) ) {
			printf("\nError: delta1_a #%d too short\n", i);
		}
		
		if (delta2_b[i] < (rof2 + rof1 + modpulse_b_delay * 0.5) ) {
			printf("\nError: delta1_b #%d too short\n", i);
		}
	}
	
	
	/* -------------- sequence start ------------- */
	
	status(A);
	
	obspower(tpwr);
	decpower(pplvl);
	setreceiver(t31);
	delay(d1);
	
	status(B);
	
	/* purge native C13 magnetization */
	if(gpurge[0] == 'y') {
		rgpulse(pw, zero, rof1, rof2);
		zgradpulse(gzlvl1, gt1);
		delay(gdelay);
		rgpulse(pw, one, rof1, rof2);
		zgradpulse(gzlvl1, gt1);
		delay(gdelay);
	}
	
	/* inept */
	decrgpulse(pp, zero, rof1, rof2);
	
	if(gpurge180[0] == 'y') {
		zgradpulse(gzlvl2, gt2);
	} else {
		delay(gt2);
	}
	
	delta_delay(delta1_a, -(rof2 + gt2 + rof1 + modpulse_a_delay * 0.5) );
	
	composite_inv(pw, cpulse_type[0]);
	
	delta_delay(delta1_b, -(rof2 + rof1 + modpulse_a_delay * 0.5) );
	
	decrgpulse(pp*2, zero, rof1, rof2);
	
	if(gpurge180[0] == 'y') {
		zgradpulse(gzlvl2, gt2);
	} else {
		delay(gt2);
	}
	
	delta_delay(delta1_c, -(rof2 + gt2 + rof1) );
	
	decrgpulse(pp, t1, rof1, rof2);
	zgradpulse(gzlvl4, gt4);
	delay(gdelay);
	rgpulse(pw, t2, rof1, rof2);
	
	/* refocusing */ 
	
	if(gpurge180[0] == 'y') {
		zgradpulse(gzlvl3, gt3);
	} else {
		delay(gt3);
	}
	
	delta_delay(delta2_a, -(rof2 + gt3 + rof1 + modpulse_b_delay * 0.5) );
	
	decrgpulse(pp*2, zero, rof1, rof2);
	
	delta_delay(delta2_b, -(rof2 + rof1 + modpulse_b_delay * 0.5) );
	
	composite_refoc(pw, cpulse_type[1]);
	
	if(gpurge180[0] == 'y') {
		zgradpulse(gzlvl3, gt3);
	} else {
		delay(gt3);
	}
	
	decpower(dpwr);
	delta_delay(delta2_c, -(rof2 + gt3 + POWER_DELAY) );
	
	status(C);
}

