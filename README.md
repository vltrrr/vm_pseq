# vm_pseq #

This repository contains various pulse sequences for Varian/Agilent spectrometers, such as:

* QEC-HSQC
* Q-HSQC
* Q-INEPT-CT

# Setup #

Copy the sequence files to your relevant vnmrsys/psglib/ and set seqfil to the corrsponding pulse sequence name though VNMRJ command line. e.g.

```
seqfil='qechsqc'
```

Then crete the needed parameters using create(), e.g.
```
create('gzlvl3')
create('cpulsetype', 'string')
```

Please look the sequence files for needed parameters and their descriptions. The publications provide more information.

# Publications #

**Quantitative, equal carbon response HSQC experiment, QEC-HSQC** Valtteri Mäkelä, Jussi Helminen, Ilkka Kilpeläinen, Sami Heikkinen  
*Journal of Magnetic Resonance*. 271, 34-49. http://dx.doi.org/10.1016/j.jmr.2016.08.003 (2016)

**Quantitative 13C NMR Spectroscopy using refocused constant-time INEPT, Q-INEPT-CT** Mäkelä, A. V. , Kilpeläinen, I. & Heikkinen, S.  
*Journal of Magnetic Resonance*. 204, 124-130 (2010)  
http://www.sciencedirect.com/science/article/pii/S1090780710000467

**Some aspects of quantitative 2D NMR**, Harri Koskela, Ilkka Kilpelainen, Sami Heikkinen,  
*Journal of Magnetic Resonance*. 174, 237-244 (2005)  
http://www.sciencedirect.com/science/article/pii/S1090780705000236


# License #

BSD 3-Clause. See LICENSE in this repo.

