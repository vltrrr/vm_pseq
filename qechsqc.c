/*

QEC-HSQC: Quantitative Equal Carbon HSQC

Parameters:
	nt		transients, min 8 (two-step phase cycle, 4 delay modulations)
	pw		proton pulse
	pwx		carbon pulse
	pwxlvl		carbon power
	gt1-gt7		PFG lengths, gt8 is automatically calculated
	gzlvl1-8	PFG powers
	phase	PFG selection, should be 1,2 for phase sensitive spectra
	gdelay		pfg recovery time
	
Flags:
	cpulsetype	y (shaka-6) | n (hard) | s (shaped) 
				shaped needs also shapex_180 and selx_pw
	purgeopt	string of three y and n chars. first two are proton 
				and carbon purges, the last is 180 purges. 
				all combinations are posiible, eg. 'nnn' 'yyy' 'nny'	

Valtteri Mäkelä / University of Helsinki

*/

#include <standard.h>

static char VERSION_STRING[MAXSTR] = "qechsqs_v10_20160620";

static int phaset1[16] = {0,2,0,2, 0,2,0,2, 0,2,0,2, 0,2,0,2};
static int phaset2[16] = {0,0,0,0, 0,0,0,0, 2,2,2,2, 2,2,2,2};
static int rcphase[16] = {0,2,0,2, 0,2,0,2, 2,0,2,0, 2,0,2,0};

static double d2_init = 0.0;

static char shape_x_inv[MAXSTR]; 
static double sel_x_pw;

void composite(double pw, char type) {
	if(type=='y') {
		/* shaka 6 */
		decrgpulse(pw*158.0/90.0, zero, rof1, 0.0);
		decrgpulse(pw*171.2/90.0, two, 0.0, 0.0);
		decrgpulse(pw*342.8/90.0, zero, 0.0, 0.0);
		decrgpulse(pw*145.5/90.0, two, 0.0, 0.0);
		decrgpulse(pw*81.2/90.0, zero, 0.0, 0.0);
		decrgpulse(pw*85.3/90.0, two, 0.0, rof2);
	} else if(type=='s') {
		decshaped_pulse(shape_x_inv, sel_x_pw, zero, rof1, 0.0);
	}
	else {
		decrgpulse(pw*2.0, zero, rof1, rof2);
	}
}

void delta_delay(double delta[], double mod) {
	/* check for too big delay modifiers */
	int i;
	for(i=0; i<4; i++) {
		if((delta[i] - mod) <= 0.0) {
			printf("Too big modifier, -> neg. delay! #%d %1.3f, mod = %1.3f\n", i+1, delta[i]*1e3, mod*1e3);
		}
	}
	
	/* counter for 2-step phase cycle */
	assign(ct,v2);
	hlv(v2,v2); /* 00 11 22 ... */
	
	/* for 4 step delay modulation */ 
	mod4(v2,v2); /* 00 11 22 33 00 ... */
	
	ifzero(v2);
		delay(delta[0] - mod);
	endif(v2);
	decr(v2);

	ifzero(v2);
		delay(delta[1] - mod);
	endif(v2);
	decr(v2);
	
	ifzero(v2);
		delay(delta[2] - mod);
	endif(v2);
	decr(v2);
	
	ifzero(v2);
		delay(delta[3] - mod);
	endif(v2);
}

pulsesequence() {
	/** -------------- variables ------------- */
	int i;
	
	/* 2d */ 
	int icosel; 
	int t1_counter; 
	int phase;
	
	/* pulses */
	double pwx;
	double pwxlvl;
	char cpulse_type[MAXSTR];
	double cpulse_length;
	
	char purge_opt[MAXSTR];
	
	/* PFG */
	double gt1;
	double gt2;
	double gt3;
	double gt4;
	double gt6;
	double gt7;
	double gt8;
	
	double gzlvl1;
	double gzlvl2;
	double gzlvl3;
	double gzlvl4;
	double gzlvl6;
	double gzlvl7;
	double gzlvl8;
	double gz_final_pwr;
	
	/* delays */
	double delta[4];
	double sigma[4];
	double delta_max;
	double sigma_max;
	double gdelay;
	
	double delta_a[4];
	double delta_b[4];
	double delta_c[4];
	double sigma_a[4];
	double sigma_b[4];
	double sigma_c[4];
	
	/** -------------- get / calc variables ------------- */
	
	/* flags */
	getstr("cpulsetype", cpulse_type);
	getstr("purgeopt", purge_opt);
	
	/* pulses */
	pwx = getval("pwx");
	pwxlvl = getval("pwxlvl");
	phase = getval("phase");
	
	getstr("shapex_180", shape_x_inv);
	sel_x_pw = getval("selx_pw");
	
	/* PFG */
	gt1 = getval("gt1"); 
	gt2 = getval("gt2");
	gt3 = getval("gt3");
	gt4 = getval("gt4");
	gt6 = getval("gt6");
	gt7 = getval("gt7");
	
	gzlvl1 = getval("gzlvl1"); 
	gzlvl2 = getval("gzlvl2"); 
	gzlvl3 = getval("gzlvl3"); 
	gzlvl4 = getval("gzlvl4"); 
	gzlvl6 = getval("gzlvl6");
	gzlvl7 = getval("gzlvl7");
	gzlvl8 = getval("gzlvl8");
	
	/* delays */
	gdelay = getval("gdelay"); 
	
	/** -------------- phases ------------- */
	
	settable(t1, 16, phaset1);
	settable(t2, 16, phaset2);
	settable(t10, 16, rcphase);
	
	/** -------------- calculations ------------- */
	
	/* decode PFG level */
	gz_final_pwr = gt4 * gzlvl4 * 2.0 * 0.2514;
	if (purge_opt[2] == 'y')
		gz_final_pwr = gz_final_pwr + (gt2 * gzlvl2);
	
	gt8 = gz_final_pwr / gzlvl8; /* length */
	
	delta[0] = 4.500e-3;
	delta[1] = 3.000e-3;
	delta[2] = 3.006e-3;
	delta[3] = 1.750e-3;
	
	sigma[0] = 2.509e-3;
	sigma[1] = 1.005e-3;
	sigma[2] = 0.500e-3;
	sigma[3] = 1.000e-3;
	
	/* determine longest delays */
	delta_max = 0.0;
	sigma_max = 0.0;
	
	for(i=0; i<4; i++) {
		if(delta[i] > delta_max) 
			delta_max = delta[i];
		if(sigma[i] > sigma_max) 
			sigma_max = sigma[i];
	}
	
	delta_max += 0.300e-3;
	sigma_max += 0.300e-3;
	
	/* calculate delta/sigma delays */
	for(i=0; i<4; i++) {
		delta_a[i] = delta_max / 2.0;
		delta_b[i] = (delta_max/2.0) - (delta[i]/2.0);
		delta_c[i] = delta[i]/2.0;
		sigma_a[i] = sigma_max / 2.0;
		sigma_b[i] = (sigma_max/2.0) - (sigma[i]/2.0);
		sigma_c[i] = sigma[i]/2.0;
	}
	
	/* composite length */
	if(cpulse_type[0]=='s') {
		cpulse_length = sel_x_pw;
	} else if(cpulse_type[0]=='y') {
		cpulse_length = pwx * 10.933333;
	} else {
		cpulse_length = pwx * 2.0;
	}
	
	/** -------------- 2D ------------- */
	
	/* invert gradient to change phase to obtain 
	hypercomplex data (phase in indirect dimension) */
	if (phase == 1) {
		icosel = +1;
	} else  {
		icosel = -1;    
	}
	
	/* create increment counter */
	if(ix == 1) {
		d2_init = d2;
	}
	t1_counter = (int) ((d2-d2_init)*sw1 + 0.5);
	
	/* add to phases in every other increment for states-TPPI */
	if(t1_counter % 2) { 
		tsadd(t1,2,4);
		tsadd(t10,2,4); 
	}
	
	/** -------------- Printout ------------- **/
	
	printf("\n%s\n\n", VERSION_STRING);
	
	printf("delta_max = %1.3f \t sigma_max = %1.3f\n", delta_max*1e3, sigma_max*1e3);
	
	printf("variable delta/sigma delays (ms):\n");
	for(i=0; i<4; i++) {
		printf("delta %d = %1.3f \t sigma %d = %1.3f\n", i, delta[i]*1e3, i, sigma[i]*1e3);
	}
	
	printf("\ncalculated delta/sigma delays (ms):\n");
	printf("del_a \tdel_b \tdel_c \tsig_a \tsig_b \tsig_c\n");
	for(i=0; i<4; i++) {
		printf("%1.3f \t", delta_a[i] * 1e3); 
		printf("%1.3f \t", delta_b[i] * 1e3);
		printf("%1.3f \t", delta_c[i] * 1e3);
		printf("%1.3f \t", sigma_a[i] * 1e3); 
		printf("%1.3f \t", sigma_b[i] * 1e3); 
		printf("%1.3f \n", sigma_c[i] * 1e3);
	}
	printf("\nrelative PFG powers:\n");
	printf("gz1 = %1.3f, %1.3fms @ %1.1f (initial purge) \n", gt1*gzlvl1, gt1*1e3, gzlvl1);
	printf("gz2 = %1.3f, %1.3fms @ %1.1f (180deg, inept purges) \n", gt2*gzlvl2, gt2*1e3, gzlvl2);
	printf("gz3 = %1.3f, %1.3fms @ %1.1f (z-purge 1)\n", gt3*gzlvl3, gt3*1e3, gzlvl3);
	printf("gz4 = %1.3f, %1.3fms @ %1.1f (encode) \n", gt4*gzlvl4, gt4*1e3, gzlvl4);
	printf("gz6 = %1.3f, %1.3fms @ %1.1f (z-purge 2)\n", gt6*gzlvl6, gt6*1e3, gzlvl6);
	printf("gz7 = %1.3f, %1.3fms @ %1.1f (180deg, refocus purges)\n", gt7*gzlvl7, gt7*1e3, gzlvl7);
	printf("gz8 = %1.3f, %1.3fms @ %1.1f (decode)\n", gt8*gzlvl8, gt8*1e3, gzlvl8);
	
	printf("\nFlags:\n");
	if (purge_opt[0] == 'y')
		printf("1H purge = yes\n");
	else
		printf("1H purge = no\n");
	if (purge_opt[1] == 'y')
		printf("13C purge = yes\n");
	else
		printf("13C purge = no\n");
	if (purge_opt[2] == 'y')
		printf("180deg pulse purges = yes\n");
	else
		printf("180deg pulse purges = no\n");
	
	printf("composite pulses (cpulsetype): "); 
	if (cpulse_type[0] == 'y' || cpulse_type[0] == 's')
		printf("yes, type = %c \n", cpulse_type[0] );
	else
		printf("no \n");
	
	printf("\npw = %1.2fus @ %1.0fdb \n", pw*1e6, tpwr);
	printf("pwx = %1.2fus @ %1.0fdb  \n", pwx*1e6, pwxlvl);
	printf("dpwr = %1.0fdb \n", dpwr); 
	
	/** -------------- checks ------------- **/
	
	if(dpwr > 56 ) { 
		text_error("don't fry the probe, dpwr too large!");
		abort(); 
	}
	
	/** -------------- sequence start ------------- */
	
	status(A);
	obspower(tpwr);
	decpower(pwxlvl);
	setreceiver(t10);
	
	/** purge 1H magnetization */
	if(purge_opt[0]=='y') {
		rgpulse(pw, zero, rof1, rof2);
		zgradpulse(0.7*gzlvl1, gt1);
		delay(gdelay);
		
		rgpulse(pw, one, rof1, rof2);
		zgradpulse(gzlvl1, gt1);
		delay(gdelay);
	}
	
	delay(d1);
	
	/** purge 13C magnetization */
	if(purge_opt[1]=='y') {
		decrgpulse(pwx, zero, rof1, rof2);
		zgradpulse(0.7*gzlvl1, gt1);
		delay(gdelay);
		
		decrgpulse(pwx, one, rof1, rof2);
		zgradpulse(gzlvl1, gt1);
		delay(gdelay);
	}
	
	/** INEPT **/
	status(B);
	
	rgpulse(pw, zero, rof1, rof2);
	
	if(purge_opt[2] == 'y') {
		zgradpulse(gzlvl2, gt2);
	} else {
		delay(gt2);
	}
	
	delta_delay(delta_a, rof2 + rof1 + gt2);
	
	rgpulse(pw*2.0, zero, rof1, rof2);
	delta_delay(delta_b, rof2 + rof1 + cpulse_length*0.5);
	composite(pwx, cpulse_type[0]);
	
	if(purge_opt[2] == 'y') {
		zgradpulse(gzlvl2, gt2);
	} else {
		delay(gt2);
	}
	
	delta_delay(delta_c, cpulse_length*0.5 + rof2 + rof1 + gt2);
	
	rgpulse(pw, one, rof1, rof2);
	zgradpulse(gzlvl3, gt3);
	delay(gdelay);
	decrgpulse(pwx, t1, rof1, rof2);
	
	/** Refocus **/
	if(purge_opt[2] == 'y') {
		zgradpulse(gzlvl7, gt7);
	} else {
		delay(gt7);
	}
	
	delta_delay(sigma_a, rof2 + rof1 + gt7);
	
	composite(pwx, cpulse_type[0]);
	delta_delay(sigma_b, rof2 + rof1 + pw);
	rgpulse(pw*2.0, zero, rof1, rof2);
	
	if(purge_opt[2] == 'y') {
		zgradpulse(gzlvl7, gt7);
	} else {
		delay(gt7);
	}
	
	delta_delay(sigma_c, pw + rof2 + gt7);
	
	/** t1 delay **/
	delay(d2/2.0);
	rgpulse(pw*2.0, zero, 0.0, 0.0);
	delay(d2/2.0);
	
	/** PFG select **/
	zgradpulse(icosel*gzlvl4, gt4);
	delay(gdelay+rof2);
	composite(pwx, cpulse_type[0]);
	zgradpulse(-1*icosel*gzlvl4, gt4);
	delay(gdelay); /* rof1 is balanced by next inept pulse */
	
	/** Reverse INEPT */
	decrgpulse(pwx, t2, rof1, rof2);
	
	zgradpulse(gzlvl6, gt6);
	delay(gdelay);
	rgpulse(pw, zero, rof1, rof2);
	
	if(purge_opt[2] == 'y') {
		zgradpulse(gzlvl2, gt2);
	} else {
		delay(gt2);
	}
	
	delta_delay(delta_a, rof2 + rof1 + gt2);
	
	rgpulse(pw*2.0, zero, rof1, rof2);
	delta_delay(delta_b, rof2 + rof1 + cpulse_length*0.5);
	composite(pwx, cpulse_type[0]);
	
	zgradpulse(gzlvl8, gt8);
	decpower(dpwr);
	
	delta_delay(delta_c, cpulse_length*0.5 + rof2 + gt8 + POWER_DELAY);
	
	/** Observe*/
	status(C);
}