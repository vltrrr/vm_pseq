/**

Q_INEPT_2D

seqmod d is the general modulation scheme wich should be used.

Valtteri Mäkelä / University of Helsinki

*/

#include <standard.h>

static char VERSION_STRING[MAXSTR] = "vm_inept_2d_v6_20120907";

/* phasetables */
static const int phi_1[2] = {1,3};
static const int phi_rec[2] = {0,2};

static const int modulations = 4; 

/* delay tables */
static double delta1[4];
static double delta1_a[4];
static double delta1_b[4];
static double delta1_c[4];

static double delta2[4];
static double delta2_a[4];
static double delta2_b[4];
static double delta2_c[4];

static double d2_init=0.0;

/** 
* create composite inversion or regular hard pulse (in inept) 
*/
void composite_inv(double pw, char type) {
	if(type=='y') {
		/* shaka6 10.933333333 x pw */
		rgpulse(pw*158.0/90.0, zero, rof1, 0.0);
		rgpulse(pw*171.2/90.0, two, 0.0, 0.0);
		rgpulse(pw*342.8/90.0, zero, 0.0, 0.0);
		rgpulse(pw*145.5/90.0, two, 0.0, 0.0);
		rgpulse(pw*81.2/90.0, zero, 0.0, 0.0);
		rgpulse(pw*85.3/90.0, two, 0.0, rof2);
	}
	else {
		rgpulse(pw*2.0, zero, rof1, rof2);
	}
}

/** create composite inversion or regular hard pulse (in refoc inept) */
void composite_refoc(double pw, char type) {
	if(type=='y') {
		/* shaka 6 */
		rgpulse(pw*158.0/90.0, zero, rof1, 0.0);
		rgpulse(pw*171.2/90.0, two, 0.0, 0.0);
		rgpulse(pw*342.8/90.0, zero, 0.0, 0.0);
		rgpulse(pw*145.5/90.0, two, 0.0, 0.0);
		rgpulse(pw*81.2/90.0, zero, 0.0, 0.0);
		rgpulse(pw*85.3/90.0, two, 0.0, rof2);
	}
	else {
		rgpulse(pw*2.0, zero, rof1, rof2);
	}
}

/** create variable delay from given delay table */
void delta_delay(double delta[], double modifier) {
	/* mod4 counter, for two step phase cycle */
	assign(ct,v2);
	
	/* for two step phasecycle */
	hlv(v2,v2); /* 00 11 22 ... */
	
	/* for 4 step delay modulation */ 
	mod4(v2,v2); /*  00 11 22 33 44 00 11 ... */
	
	ifzero(v2);
	delay(delta[0] + modifier);
	endif(v2);
	decr(v2);
	
	ifzero(v2);
	delay(delta[1] + modifier);
	endif(v2);
	decr(v2);
	
	ifzero(v2);
	delay(delta[2] + modifier);
	endif(v2);
	decr(v2);
	
	ifzero(v2);
	delay(delta[3] + modifier);
	endif(v2);
}

void pulsesequence() {
	/* -------------- variables ------------- */
	
	/* flags */
	char seq_mod[MAXSTR];
	char cpulse_type[MAXSTR];
	char gpurge[MAXSTR];
	char gpurge180[MAXSTR];
	char printout[MAXSTR];
	
	/* gradients */
	double gzlvl1;
	double gzlvl2;
	double gzlvl3;
	double gt1;
	double gt2;
	double gt3;
	
	/* pulses */
	double pw2;
	double pp;
	double pplvl;
	double pp2;
	
	/* delays */
	double gdelay;
	
	double modpulse_a_delay;
	double modpulse_b_delay;
	
	double delta1_max;
	double delta2_max;
	double delta_max_adddelay;
	
	/* 2D */
	double d2evol;
	
	int phase;
	int t1_counter;
	
	/* parameters & other */
	double jhx1;
	double jhx2;
	
	double delta1_delay;
	double delta2_delay;
	
	int i;
	int pulseseqmod_ok;
	
	/* -------------- get / calc variables ------------- */
	
	gdelay = 150.0e-6;
	delta_max_adddelay = 0.300e-3;
	
	/* --- flags --- */
	
	getstr("gpurge",gpurge);
	getstr("gpurge180",gpurge180);
	getstr("seqmod",seq_mod);
	getstr("cpulsetype",cpulse_type);
	getstr("printout",printout);
	
	/* --- gradients --- */
	
	gzlvl1 = gzlvl2 = gzlvl3 = 0;
	gt1 = gt2 = gt3 = 0;
	
	if(gpurge[0] == 'y') {
		gzlvl1 = getval("gzlvl1");
		gt1 = getval("gt1");
	}
	
	if(gpurge180[0] == 'y') {
		gzlvl2 = getval("gzlvl2");
		gzlvl3 = getval("gzlvl3");	
		gt2 = getval("gt2");
		gt3 = getval("gt3");
	}
	
	/* --- pulses --- */
	
	pw2 = pw*2;
	pp = getval("pp");
	pplvl = getval("pplvl");
	pp2 = pp*2;
	
	/* --- phases --- */
	
	settable(t1,2,phi_1);
	settable(t31,2,phi_rec);
	
	/* --- delays --- */
	
	jhx1 = jhx2 = delta1_delay = delta2_delay = 0;
	pulseseqmod_ok = 0;
	
	if(seq_mod[0] == 'n') {
		
		jhx1 = getval("jhx1");
		jhx2 = getval("jhx2");
		delta1_max = getval("delta1max");
		delta2_max = getval("delta2max");
		
		for(i=0; i<modulations;i++) {
			if (jhx1 == 0) {
				delta1_delay = getval("delta1delay");
				delta1[i] = delta1_delay;
			}
			else {
				delta1[i] = 1.0 /( 2.0 * jhx1);
			}
			
			if (jhx2 == 0) {
				delta2_delay = getval("delta2delay");
				delta2[i] = delta2_delay;
			}
			else {
				delta2[i] = 1.0 /( 3.0 * jhx2);
			}
		}
	}
	else {
		if(seq_mod[0] == 'c') {
			/* 115-170 12.9 coarse % */
			delta1[0] = 5.0e-3;
			delta1[1] = 2.2e-3;
			delta1[2] = 1.2e-3;
			delta1[3] = 1.0e-3;
			
			delta2[0] = 3.4e-3;
			delta2[1] = 1.8e-3;
			delta2[2] = 1.4e-3;
			delta2[3] = 1.6e-3;
			
			pulseseqmod_ok = 1;
		} 
		else if(seq_mod[0] == 'd') {
			/* 115-170 12.74 */
			delta1[0] = 5.0000e-3;
			delta1[1] = 2.1949e-3;
			delta1[2] = 1.2027e-3;
			delta1[3] = 1.0020e-3;
			
			delta2[0] = 3.4133e-3;
			delta2[1] = 1.7875e-3;
			delta2[2] = 1.3991e-3;
			delta2[3] = 1.5967e-3;
			
			pulseseqmod_ok = 1;
		}
		else if(seq_mod[0] == 'e') {
			/* 120-130 4.24 */
			delta1[0] = 5.0e-3;
			delta1[1] = 1.6e-3;
			delta1[2] = 1.0e-3;
			delta1[3] = 1.0e-3;
			
			delta2[0] = 3.4e-3;
			delta2[1] = 1.2e-3;
			delta2[2] = 1.8e-3;
			delta2[3] = 1.6e-3;
			
			pulseseqmod_ok = 1;
		}	
		else {
			printf("invalid modulation selected (seqmod = '%c')\n", seq_mod[0]); 
			abort();
		}
		
		/* determine longest delays */
		delta1_max = 0;
		delta2_max = 0;
		for(i=0; i<modulations; i++) {
			if(delta1[i] > delta1_max) 
				delta1_max = delta1[i];
			if(delta2[i] > delta2_max) 
				delta2_max = delta2[i];
		}
		
		delta1_max += delta_max_adddelay;
		delta2_max += delta_max_adddelay;
	}
	
	modpulse_a_delay = pw * 2.0;
	modpulse_b_delay = pp * 2.0;
	
	/* composite pulse lenghts to be added to the first evolution */
	if(cpulse_type[0]=='y') {
		modpulse_a_delay = pw * 10.933333;
	} 
	
	/* calculate delta1_a/b and delta2_a/b from delta arrays */ 
	for (i=0; i<modulations; i++) {
		delta1_a[i] = delta1[i] / 2.0;
		delta1_b[i] = delta1_max / 2.0 - delta1_a[i];
		delta1_c[i] = delta1_max / 2.0;
		
		delta2_a[i] = delta2[i] / 2.0;
		delta2_b[i] = delta2_max / 2.0 - delta2_a[i];
		delta2_c[i] = delta2_max / 2.0;
	}
	
	/* --- 2D --- */
	
	/* create increment counter */
	if(ix == 1) {
		d2_init = d2;
	}
	t1_counter = (int) ((d2-d2_init)*sw1 + 0.5);
	
	/* add to phases in every other increment for states-TPPI */
	if(t1_counter % 2) { 
		tsadd(t1,2,4);
		tsadd(t31,2,4); 
	}
	
	/* phase for hypercomplex data */
	phase = getval("phase");
	
	if ( phase == 2 ) {
		tsadd(t1,1,4);
	} 
	
	/* evolution time */
	d2evol = d2/2.0;
	
	/* --- printout --- */
	
	printf("\n%s\n\n",VERSION_STRING);
	
	if(printout != NULL && printout[0] == 'y') {
		
		/* Pulses */
		printf("pw = %1.2fus @ %1.0fdb \n", pw*1e6, tpwr);
		printf("pp = %1.2fus @ %1.0fdb  \n", pp*1e6, pplvl);
		printf("dpwr = %1.0fdb \n", dpwr); 
		
		printf("composite pulse in delta1 (cpulsetype[0]): "); 
		if (cpulse_type[0] == 'y') {
			printf("yes \n");
		} else {
			printf("no \n");
		}
		
		printf("composite pulse in delta2 (cpulsetype[1]): ");
		if (cpulse_type[1] == 'y') {
			printf("yes \n");
		} else {
			printf("no \n");
		}
		
		/* PFG */
		printf("\n");
		printf("Purge mangetization (gpurge) = ");
		if (gpurge[0] == 'y') {
			printf("yes\n");
		} else {
			printf("no\n");
		}
		
		printf("Purge gradients around inversion and refocusing pulses (gpurge180) = ");
		if (gpurge180[0] == 'y') {
			printf("yes\n");
		} else {
			printf("no\n");
		}
		
		/* Modulation */
		printf("\nPulse sequence modulation: ");
		if (pulseseqmod_ok == 1) {
			printf("yes, using modulation %c \n", seq_mod[0]);
			printf("delta1_max %1.3fms\t delta2_max %1.3fms\t \n", delta1_max*1e3, delta2_max*1e3);
			
			printf("\nvariable delays (ms) \n"); 
			printf("# d_1a\t d1_b\t d_1c\t total\t d_2a\t d_2b\t d_2c\t total\n"); 
			
			for (i=0; i<modulations; i++) {
				printf("%u ", i);
				printf("%1.3f\t ", delta1_a[i]*1e3);
				printf("%1.3f\t ", delta1_b[i]*1e3);
				printf("%1.3f\t ", delta1_c[i]*1e3);
				printf("%1.3f\t ", (delta1_a[i] + delta1_b[i] + delta1_c[i])*1e3);
				printf("%1.3f\t ", delta2_a[i]*1e3);
				printf("%1.3f\t ", delta2_b[i]*1e3);
				printf("%1.3f\t ", delta2_c[i]*1e3);
				printf("%1.3f\n", (delta2_a[i] + delta2_b[i] + delta2_c[i] )*1e3);
			}

		} 
		else {
			printf("not modulated, using static evolution times: \n");
			
			printf("  delta1 = %1.2fHz / %1.3fms, delta1max = %1.3fms, ", 1.0/(2.0*delta1[0]), delta1[0] * 1e3, delta1_max*1e3 );
			if (jhx1 != 0) {
				printf("set using jhx1 \n");
			} else {
				printf("set using delta1delay (jhx1 is set to 0)\n");
			}
			
			printf("  delta2 = %1.2fHz / %1.3fms, delta2max = %1.3fms, ", 1.0/(2.0*delta2[0]), delta2[0] * 1e3, delta2_max*1e3 );
			if (jhx2 != 0) {
				printf("set using jhx2 \n");
			} else {
				printf("set using delta2delay (jhx2 is set to 0)\n");
			}
			
			printf("calculated delays: \n");
			printf("  delta1_a=%1.3f  ", delta1_a[0]*1e3);
			printf("delta1_b=%1.3f  ", delta1_b[0]*1e3);
			printf("delta1_c=%1.3f  ", delta1_c[0]*1e3);
			printf("delta1_total %1.3f \n", (delta1_a[0] + delta1_b[0] + delta1_c[0])*1e3);
			
			printf("  delta2_a=%1.3f  ", delta2_a[0]*1e3);
			printf("delta2_b=%1.3f  ", delta2_b[0]*1e3);
			printf("delta2_c=%1.3f  ", delta2_c[0]*1e3);
			printf("elta2_total %1.3f \n", (delta2_a[0] + delta2_b[0] + delta2_c[0])*1e3);
		}
		
		printf("gdelay = %1.0fus, ", gdelay*1e6);
		printf("rof1 = %1.3fms, rof2 = %1.3fms\n", rof1*1e3, rof2*1e3);
	}
	
	/* --- checks --- */
	
	for (i=0; i<modulations; i++) {
		
		/* eg. 250us gradient + 150us grecovery + 100us max for composite = 500us */ 
		if (delta1_a[i] < (rof2 + gt2 + rof1 + modpulse_a_delay * 0.5) ) {
			printf("\nError: delta1_a #%d too short\n", i);
			abort();
		}
		
		if (delta1_b[i] < (rof2 + rof1 + modpulse_a_delay * 0.5) ) {
			printf("\nError: delta1_b #%d too short\n", i);
			abort();
		}
		
		if (delta2_a[i] < (rof2 + gt3 + rof1 + modpulse_b_delay * 0.5) ) {
			printf("\nError: delta1_a #%d too short\n", i);
			abort();
		}
		
		if (delta2_b[i] < (rof2 + rof1 + modpulse_b_delay * 0.5) ) {
			printf("\nError: delta1_b #%d too short\n", i);
			abort();
		}
	}
	
	/* -------------- sequence start ------------- */
	
	status(A);
	
	obspower(tpwr);
	decpower(pplvl);
	setreceiver(t31);
	delay(d1);
	
	status(B);
	
	/* purge native C13 magnetization */
	if(gpurge[0] == 'y') {
		rgpulse(pw, zero, rof1, rof2);
		zgradpulse(gzlvl1, gt1);
		delay(gdelay);
		rgpulse(pw, one, rof1, rof2);
		zgradpulse(gzlvl1, gt1);
		delay(gdelay);
	}
	
	/* inept */
	decrgpulse(pp, zero, rof1, rof2);
	
	if(gpurge180[0] == 'y') {
		zgradpulse(gzlvl2, gt2);
	} else {
		delay(gt2);
	}
	
	delta_delay( delta1_a, -(rof2 + gt2 + rof1 + modpulse_a_delay * 0.5) + d2evol );
	
	composite_inv(pw, cpulse_type[0]);
	
	delta_delay( delta1_b, -(rof2 + rof1 + modpulse_a_delay * 0.5) + d2evol );
	
	decrgpulse(pp2, zero, rof1, rof2);
	
	if(gpurge180[0] == 'y') {
		zgradpulse(gzlvl2, gt2);
	} else {
		delay(gt2);
	}
	
	delta_delay(delta1_c, -(rof2 + gt2 + rof1) );
	
	simpulse(pw, pp, zero, t1, rof1, rof2);
	
	/* refocusing */ 
	
	if(gpurge180[0] == 'y') {
		zgradpulse(gzlvl3, gt3);
	} else {
		delay(gt3);
	}
	
	delta_delay(delta2_a, -(rof2 + gt3 + rof1 + modpulse_b_delay * 0.5) );
	
	decrgpulse(pp2, zero, rof1, rof2);
	
	delta_delay(delta2_b, -(rof2 + rof1 + modpulse_b_delay * 0.5) );
	
	composite_refoc(pw, cpulse_type[1]);
	
	if(gpurge180[0] == 'y') {
		zgradpulse(gzlvl3, gt3);
	} else {
		delay(gt3);
	}
	
	decpower(dpwr);
	delta_delay(delta2_c, -(rof2 + gt3 + POWER_DELAY) );
	
	status(C);
}
