/**
Q-HSQC as in:

Harri Koskela, Ilkka Kilpelainen, Sami Heikkinen, Some aspects of quantitative 2D NMR, 
Journal of Magnetic Resonance, Volume 174, Issue 2, June 2005, Pages 237-244

Regular sequence, no XY-16. QHSQC2 is probably a better implementation for simple QHSQC..

Cleaned up version from 2005 code. 

minimum nt = 2 phases * 4 delays = 8

parameters:
	
	pwx = carbon pulse
	pwxlvl = carbon power
	cpulsetype = composite pulse type. n = off, a = regular, b = shaka
	phase = 1|2, phase selection for hypercomplex data
	
	PFG:
	gt1 and gzlvl1 = C natural purge
	gt2 and gzlvl2 = 180 purge in inept/revinept
	gt3 and gzlvl3 = z purge after inept
	gt4 and gzlvl4 = encode (carbon), used twice (power is doubled)
	gt6 and gzlvl6 = z purge before inept
	gt8 and gzlvl8 = decode (proton)
	
	gdelay = gradient eddy delay, in us

Valtteri Mäkelä / University of Helsinki

*/

#include <standard.h>

static char VERSION_STRING[MAXSTR] = "qhsqcv8_20110909";

static int pphase3[2] = {0,2};
static int rcphase[2] = {0,2};

static double d2_init = 0.0;

void composite(double pw, char type) {
	if(type=='a') {
		decrgpulse(pw*1.0, zero, rof1, 0.0);
		decrgpulse(pw*2.0, one, 0.0, 0.0);
		decrgpulse(pw*1.0, zero, 0.0, rof2);
	} 
	else if(type=='b') {
		/* shaka 6 */
		decrgpulse(pw*158.0/90.0, zero, rof1, 0.0);
		decrgpulse(pw*171.2/90.0, two, 0.0, 0.0);
		decrgpulse(pw*342.8/90.0, zero, 0.0, 0.0);
		decrgpulse(pw*145.5/90.0, two, 0.0, 0.0);
		decrgpulse(pw*81.2/90.0, zero, 0.0, 0.0);
		decrgpulse(pw*85.3/90.0, two, 0.0, rof2);
	}
	else {
		decrgpulse(pw*2.0, zero, rof1, rof2);
	}
}

void delta_delay(double delta[], double modifier) {
	/* counter for 2-step phase cycle */
	assign(ct,v2);
	hlv(v2,v2); /* 00 11 22 ... */
	
	/* for 4 step delay modulation */ 
	mod4(v2,v2); /* 00 11 22 33 00 ... */
	
	ifzero(v2);
		delay(delta[0] - modifier);
	endif(v2);
	decr(v2);

	ifzero(v2);
		delay(delta[1] - modifier);
	endif(v2);
	decr(v2);
	
	ifzero(v2);
		delay(delta[2] - modifier);
	endif(v2);
	decr(v2);
	
	ifzero(v2);
		delay(delta[3] - modifier);
	endif(v2);
}

pulsesequence() {
	/** -------------- variables ------------- */

	int i;
	
	/* 2d */ 
	int icosel; 
	int t1_counter; 
	int phase;
	
	/* pulses */
	double pwx;
	double pwxlvl;
	char cpulse_type[MAXSTR];
	double cpulse_length;
	
	/* PFG */
	double gt1;
	double gt2;
	double gt3;
	double gt4;
	double gt6;
	double gt8;
	
	double gzlvl1;
	double gzlvl2;
	double gzlvl3;
	double gzlvl4;
	double gzlvl6;
	double gzlvl8;
	
	/* delays */
	double delta[4];
	double dmax;
	double gdelay;
	
	double delta_a[4];
	double delta_b[4];
	double delta_c[4];
	
	/** -------------- get / calc variables ------------- */
	
	/* pulses */
	pwx = getval("pwx");
	pwxlvl = getval("pwxlvl");
	getstr("cpulsetype", cpulse_type);
	
	phase = getval("phase");
	
	/* PFG */
	gt1 = getval("gt1"); /* C natural purge */
	gt2 = getval("gt2"); /* 180 purge in inept/revinept */
	gt3 = getval("gt3"); /* z purge */
	gt4 = getval("gt4"); /* encode */
	gt6 = getval("gt6"); /* z purge 2 */
	gt8 = getval("gt8"); /* decode */
	
	gzlvl1 = getval("gzlvl1");
	gzlvl2 = getval("gzlvl2");
	gzlvl3 = getval("gzlvl3");
	gzlvl4 = getval("gzlvl4");
	gzlvl6 = getval("gzlvl6");
	gzlvl8 = getval("gzlvl8");
	
	gdelay = getval("gdelay"); 
	
	
	/** -------------- phases ------------- */
	
	settable(t3, 2, pphase3);
	settable(t10, 2, rcphase);
	
	/** -------------- calculations ------------- */
	
	dmax = 6.1e-3; /* 5.92e-3; */
	delta[0] = 2.94e-3;
	delta[1] = 2.94e-3;
	delta[2] = 2.94e-3;
	delta[3] = 5.92e-3;
	
	/* calculate delta delays */
	for(i=0; i<4; i++) {
		delta_a[i] = dmax/2.0;
		delta_b[i] = (dmax/2.0) - (delta[i]/2.0);
		delta_c[i] = delta[i]/2.0;
	}
	
	/* composite length */
	if(cpulse_type[0]=='a') {
		cpulse_length = pwx * 4.0;
	} else if(cpulse_type[0]=='b') {
		cpulse_length = pwx * 10.933333;
	} else {
		cpulse_length = pwx * 2.0;
	}
	
	/** -------------- 2D ------------- */
	
	/* invert gradient to change phase to obtain 
	hypercomplex data (phase in indirect dimension) */
	if (phase == 1) {
		icosel = +1;
	} else  {
		icosel = -1;    
	}
	
	/* create increment counter */
	if(ix == 1) {
		d2_init = d2;
	}
	t1_counter = (int) ((d2-d2_init)*sw1 + 0.5);
	
	/* add to phases in every other increment for states-TPPI */
	if(t1_counter % 2) { 
		tsadd(t3,2,4);
		tsadd(t10,2,4); 
	}

	/** -------------- Printout ------------- **/
	
	printf("\n%s\n\n", VERSION_STRING);
	printf("delta delays (ms):\n");
		
	printf("delta_max = %1.3f \n", dmax * 1e3);
	for(i=0; i<4 ;i++) {
		printf("delta %d = %1.3f\n", i, delta[i]*1e3);
	}
	printf("\ncalculated delta delays (ms):\n");
	printf("a\t b\t c\n");
	for(i=0; i<4; i++) {
		printf("%1.3f \t", delta_a[i] * 1e3); 
		printf("%1.3f \t", delta_b[i] * 1e3);
		printf("%1.3f \n", delta_c[i] * 1e3);
	}
	
	/** -------------- checks ------------- **/
	
	if(dpwr > 56 ) { 
		text_error("don't fry the probe, dpwr too large!");
		abort(); 
	}
	
	/** -------------- sequence start ------------- */
	
	status(A);
	obspower(tpwr);
	decpower(pwxlvl);
	setreceiver(t10);
	delay(d1);
	
	/** purge magnetization */
	status(B);
	decrgpulse(pwx, zero, rof1, rof2);
	zgradpulse(gzlvl1, gt1);
	delay(gdelay);
	
	decrgpulse(pwx, one, rof1, rof2);
	zgradpulse(0.7*gzlvl1, gt1);
	delay(gdelay);
	
	/** Inept **/
	rgpulse(pw, zero, rof1, rof2);
	zgradpulse(gzlvl2, gt2);
	
	delta_delay(delta_a, gt2 + rof2 + rof1);
	rgpulse(pw*2.0, zero, rof1, rof2);
	delta_delay(delta_b, cpulse_length/2.0 + rof2 + rof1);
	
	composite(pwx, cpulse_type[0]);
	
	zgradpulse(gzlvl2, gt2);
	delta_delay(delta_c, gt2 + cpulse_length/2.0 + rof2 + rof1);
	
	rgpulse(pw, one, rof1, rof2);
	zgradpulse(gzlvl3, gt3);
	delay(gdelay);
	
	/** Evolution */
	decrgpulse(pwx, t3, rof1, 0.0);
	
	delay(d2/2);
	rgpulse(pw*2.0, zero, 0.0, 0.0);
	delay(d2/2);
	
	zgradpulse(icosel*gzlvl4, gt4);
	delay(gdelay);
	delay(rof2);
	
	composite(pwx, cpulse_type[0]);
	
	zgradpulse(-1*icosel*gzlvl4, gt4);
	delay(gdelay);
	
	decrgpulse(pwx, zero, rof1, rof2);
	zgradpulse(gzlvl6, gt6);
	delay(gdelay);
	
	/** Reverse Inept */
	rgpulse(pw, zero, rof1, rof2);
	zgradpulse(gzlvl2, gt2);
	
	delta_delay(delta_a, gt2 + rof2 + rof1);
	rgpulse(pw*2.0, zero, rof1, rof2);
	delta_delay(delta_b, cpulse_length/2.0 + rof2 + rof1);
	
	composite(pwx, cpulse_type[0]);
	
	zgradpulse(gzlvl2, gt2);
	delay(gdelay);
	zgradpulse(gzlvl8, gt8);
	decpower(dpwr);
	delta_delay(delta_c, cpulse_length/2.0 + rof2 + gt2 + gdelay + gt8 + POWER_DELAY);
	
	/** Observe*/
	status(C);
}
