/**

Q-CAHSQC with CPMG as in:

Harri Koskela, Ilkka Kilpelainen, Sami Heikkinen, Some aspects of quantitative 2D NMR, 
Journal of Magnetic Resonance, Volume 174, Issue 2, June 2005, Pages 237-244

Cleaned up version from 2005 code.

minimum nt = 2 phases * 4 delays = 8

parameters:
	
	pwx = carbon pulse
	pwxlvl = carbon power
	cpulsetype = composite pulse type. n = off, a = regular, b = shaka
	phase = 1|2, phase selection for hypercomplex data
	
	PFG:
	gt1 and gzlvl1 = encode (carbon), used twice (power is doubled)
	gt2 and gzlvl2 = decode (proton)
	sgt1 and sgzlvl1 = C natural purge
	sgt2 and sgzlvl2 = z purge after/before inept
	
	gdelay = gradient eddy delay, in us


Valtteri Mäkelä / University of Helsinki

*/

#include <standard.h>

static char VERSION_STRING[MAXSTR] = "qcahsqc_v7_2016_05_13";

/* phasetables */
static int pphase1[2] = {0,2};
static int pphase3[16] = {0,1,0,1,1,0,1,0,2,3,2,3,3,2,3,2};
static int rcphase[2] = {0,2};

static double d2_init = 0.0;

void composite(double pw, char type) {
	if(type=='a') {
		decrgpulse(pw*1.0, zero, rof1, 0.0);
		decrgpulse(pw*2.0, one, 0.0, 0.0);
		decrgpulse(pw*1.0, zero, 0.0, rof2);
	} 
	else if(type=='b') {
		/* shaka 6 */
		decrgpulse(pw*158.0/90.0, zero, rof1, 0.0);
		decrgpulse(pw*171.2/90.0, two, 0.0, 0.0);
		decrgpulse(pw*342.8/90.0, zero, 0.0, 0.0);
		decrgpulse(pw*145.5/90.0, two, 0.0, 0.0);
		decrgpulse(pw*81.2/90.0, zero, 0.0, 0.0);
		decrgpulse(pw*85.3/90.0, two, 0.0, rof2);
	}
	else {
		decrgpulse(pw*2.0, zero, rof1, rof2);
	}
}

pulsesequence() {
	/* -------------- variables ------------- */
	
	/* 2d */ 
	int icosel; 
	int t1_counter; 
	int phase;
	
	/* pulses */
	double pwx;
	double pwxlvl;
	char cpulse_type[MAXSTR];
	
	/* PFG */
	double gt1;
	double gt2;
	double sgt1;
	double sgt2;
	
	double gzlvl1;
	double gzlvl2;
	double sgzlvl1;
	double sgzlvl2;
	
	double gdelay; 
	
	/* delays */
	double xypad; 
	double xylength;
	double deltamax;
	double maxpw; 
	
	/* -------------- get / calc variables ------------- */
	
	phase = getval("phase");
	
	/* pulses */
	pwx = getval("pwx");
	pwxlvl = getval("pwxlvl");
	getstr("cpulsetype", cpulse_type);
	
	/* PFG */
	gt1 = getval("gt1");
	gt2 = getval("gt2");
	sgt1 = getval("sgt1");
	sgt2 = getval("sgt2");
	
	gzlvl1 = getval("gzlvl1");
	gzlvl2 = getval("gzlvl2");
	sgzlvl1 = getval("sgzlvl1");
	sgzlvl2 = getval("sgzlvl2");
	
	gdelay = getval("gdelay"); 
	
	/* -------------- phases ------------- */
	
	/* set phasetables */
	settable(t1, 2, pphase1);
	settable(t3, 16, pphase3);
	settable(t10, 2, rcphase);
	
	/* set autoincrement to xy16 table so that it cycles trough automagically. */
	setautoincrement(t3);
	
	/* -------------- 2D ------------- */
	
	/* invert gradient to change phase to obtain 
	hypercomplex data (phase in indirect dimension) */
	if (phase == 1) {
		icosel = +1;
	} else  {
		icosel = -1;    
	}
	
	/* create increment counter */
	if(ix == 1) {
		d2_init = d2;
	}
	t1_counter = (int) ((d2-d2_init)*sw1 + 0.5);
	
	/* add to phases in every other increment for states-TPPI */
	if(t1_counter % 2) { 
		tsadd(t1,2,4);
		tsadd(t10,2,4); 
	}
	
	/* -------------- calculations ------------- */
	
	deltamax = 2.94e-3;
	
	/* xy pulse padding
	pad xy16 simpulses not containing carbon pulse with delay, so simpulses
	will be identical in length. first, calculate padding:
	*/
	if((pwx*2) > (pw*2)) { 
		xypad = (pwx*2 - pw*2) / 2;
		maxpw = pwx*2;
	} else {
		/* no padding needed, because carbon 180 pulse doesn't alter pulse duration
		(because H 180 is longer and dominates) */
		xypad = 0.0;
		maxpw = pw*2;
	}
	
	tau = deltamax/32 - 0.5 * maxpw;
	xylength = 16 * (2*tau + maxpw);
	
	/* -------------- realtime variable initializations ------------- */
	
	/* xy pulse series selection
	select XY-16 every 4th transient. 
	Prepare v1 for usage with ifzero */
	assign(ct,v2);
	incr(v2); /* 1234567.. */
	incr(v2); /* 2345678 */
	hlv(v2,v2); /* 1122334455*/
	mod4(v2,v1); /*1122330011223300*/
	
	/* xy loop count */
	initval(16, v10);
	
	/** -------------- Printout ------------- **/
	
	printf("\n%s\n\n", VERSION_STRING);
	
	printf("deltamax = %1.3f ms\n", deltamax * 1e3); 
	printf("tau = %1.3f us \n", tau * 1e6); 
	printf("xypad = %1.3f us \n", xypad * 1e6);
	
	/** -------------- checks ------------- **/
	
	if(dpwr > 56 ) { 
		text_error("don't fry the probe, dpwr too large!");
		abort(); 
	} 
	
	if(tau < maxpw || tau < 0) { 
		text_error("don't fry the probe, tau too small");
		abort(); 
	}
	
	/** -------------- sequence start ------------- */
	
	status(A);
	
	setreceiver(t10);
	obspower(tpwr);
	decpower(pwxlvl);
	delay(d1);
	
	/** purge magnetization */
	status(B);
	decrgpulse(pwx, zero, rof1, rof2);
	zgradpulse(sgzlvl1, sgt1);
	delay(gdelay);
	
	decrgpulse(pwx, one, rof1, rof2);
	zgradpulse(0.7*sgzlvl1, sgt1);
	delay(gdelay);
	
	/** INEPT **/
	rgpulse(pw, zero, rof1, 0.0);

	/* XY-16 loop #1 */
	loop(v10, v11);
		getelem(t3, v8, v7);
		delay(tau);
		
		ifzero(v1);
			simpulse(pw*2, pwx*2, v7, v7, 0.0, 0.0);
		
		elsenz(v1);
			delay(xypad);
			simpulse(pw*2, 0.0, v7, v7, 0.0, 0.0);
			delay(xypad);
		endif(v1);
		
		delay(tau);
	endloop(v11);
	
	/* XY-16 loop #2 */
	loop(v10, v11);
		getelem(t3, v8, v7);
		delay(tau);
		simpulse(pw*2, pwx*2, v7, v7, 0.0, 0.0);
		delay(tau);
	endloop(v11);
	
	/** Evolution */
	rgpulse(pw, one, 0.0, rof2);
	zgradpulse(0.7*sgzlvl2, sgt2);
	delay(gdelay);
	decrgpulse(pwx, t1, rof1, 0.0);
	
	delay(d2/2);
	rgpulse(pw*2, zero, 0.0, 0.0);
	delay(d2/2);
	
	zgradpulse(icosel*gzlvl1, gt1);
	delay(gdelay);
	delay(rof2);
	composite(pwx, cpulse_type[0]);
	zgradpulse(-1*icosel*gzlvl1, gt1);
	delay(gdelay);
	
	decrgpulse(pwx, zero, rof1, rof2);
	zgradpulse(sgzlvl2, sgt2);
	delay(gdelay);
	
	/** Reverse INEPT */
	rgpulse(pw, zero, rof1, 0.0);
	/* XY-16 loop #3 */
	loop(v10, v11);
		getelem(t3, v8, v7);
		delay(tau);
		
		ifzero(v1);
			simpulse(pw*2, pwx*2, v7, v7, 0.0, 0.0);
		
		elsenz(v1);
			delay(xypad);
			simpulse(pw*2, 0.0, v7, v7, 0.0, 0.0);
			delay(xypad);
		endif(v1);
		
		delay(tau);
	endloop(v11);
	
	/* XY-16 loop #4 */
	loop(v10, v11);
		getelem(t3, v8, v7);
		
		delay(tau);
		simpulse(pw*2, pwx*2, v7, v7, 0.0, 0.0);
		delay(tau);
	endloop(v11);
	
	delay(gt2 + POWER_DELAY + gdelay + rof2);
	
	rgpulse(pw*2, zero, rof1, rof2);
	
	zgradpulse(gzlvl2, gt2);
	decpower(dpwr);
	delay(gdelay + rof1);
	
	/* C */
	status(C);
}